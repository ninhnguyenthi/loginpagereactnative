import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/dist/FontAwesome';

export default function InputComponent(props) {
  const [value, setValue] = useState('');
  const [visible, setVisible] = useState(true);
  const HandleOnPress = () => {
    setVisible(visible => !visible);
  };
  return (
    <View style={styles.inputView}>
      <IconAntDesign name={props.iconName} size={20} style={{marginLeft: 10}} />
      <TextInput
        secureTextEntry={props.type === 'password' ? visible : false}
        value={value}
        style={styles.inputText}
        placeholder={props.placeholder}
        onChangeText={text => setValue(text)}
      />
      {props.type === 'password' ? (
        <TouchableOpacity onPress={HandleOnPress}>
          <IconAntDesign
            style={styles.icon}
            name={visible ? 'eye' : 'eye-slash'}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
}
const TEXT = {
  color: '#000',
};
const CENTER = {
  alignItems: 'center',
  justifyContent: 'center',
};

const styles = StyleSheet.create({
  inputView: {
    marginHorizontal: 30,
    borderRadius: 10,
    marginTop: 30,
    borderWidth: 2,
    borderColor: '#EAEAEA',
    flexDirection: 'row',
    alignItems: 'center',
    // placeholderTextColor: '#FF000000',
  },
  inputText: {
    flex: 1,
    paddingRight: 20,
    fontSize: 17,
  },
  icon: {
    paddingRight: 20,
    fontSize: 17,
  },
});
