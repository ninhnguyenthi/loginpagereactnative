import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import InputComponent from './InputComponent';

export default function LoginScreen() {
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const [visible, setVisible] = useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{...CENTER, paddingVertical: 30, backgroundColor: '#04A844'}}>
        <Text style={styles.heading}>ĐĂNG NHẬP</Text>
      </View>
      <View style={styles.image}>
        <Image source={require('../images/logo.png')} />
      </View>
      <InputComponent type="" iconName="phone" placeholder="Số điện thoại" />
      <InputComponent type="password" iconName="lock" placeholder="Mật khẩu" />
      <TouchableOpacity style={styles.loginButtonContainer}>
        <Text style={styles.loginBtn}>ĐĂNG NHẬP</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.forgotPassword}>
        <Text style={styles.forgotPassword}>Quên mật khẩu?</Text>
      </TouchableOpacity>
      <View style={styles.account}>
        <Text>Bạn chưa có tài khoản?</Text>
        <TouchableOpacity style={styles.registerContainer}>
          <Text style={styles.registerContainer}>Đăng ký ngay</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}
const TEXT = {
  color: '#000',
};
const CENTER = {
  alignItems: 'center',
  justifyContent: 'center',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  heading: {
    ...CENTER,
    color: '#fff',
    fontSize: 15,
  },
  image: {
    ...CENTER,
    textAlign: 'center',
    marginTop: 50,
    marginBottom: 20,
  },
  inputView: {
    marginHorizontal: 30,
    borderRadius: 10,
    marginTop: 30,
    borderWidth: 2,
    borderColor: '#EAEAEA',
    flexDirection: 'row',
    alignItems: 'center',
    // placeholderTextColor: '#FF000000',
  },
  inputText: {
    paddingRight: 20,
    fontSize: 17,
  },
  loginBtn: {
    marginTop: 50,
    color: '#fff',
    backgroundColor: '#04A844',
    borderRadius: 25,
    paddingHorizontal: 130,
    paddingVertical: 10,
    fontWeight: 'bold',
  },
  loginButtonContainer: {
    ...CENTER,
  },
  forgotPassword: {
    ...TEXT,
    ...CENTER,
    marginTop: 10,
    textDecorationLine: 'underline',
  },
  account: {
    flexDirection: 'row',
    ...CENTER,
    marginTop: 100,
  },
  registerContainer: {
    color: '#04A844',
    textDecorationLine: 'underline',
    marginLeft: 5,
  },
});
